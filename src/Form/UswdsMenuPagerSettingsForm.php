<?php

namespace Drupal\uswds_menu_pager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
/**
 * Configure Telehealth Search settings using searchgov.
 */
class UswdsMenuPagerSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uswds_menu_pager_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'uswds_menu_pager.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('uswds_menu_pager.settings');

    $form['uswds_menu_pager_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
      'hide_description' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide Description?'),
        '#default_value' => $settings->get('hide_description'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('uswds_menu_pager.settings');

    $config
      ->set('hide_description', $form_state->getValue('hide_description'))
      ->save();
  }
}
