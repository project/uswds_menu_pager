CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Note

INTRODUCTION
------------

Menu Pager adds a block for each menu that adds previous and next link
navigation based on the current page.

USWDS Menu Pager extends this functionality to incorporate USWDS design
and enable the display of the value stored in each menu entity's 
"Description" field.

To hide the description field from display go to the settings form at 
/admin/config/menu_pager/uswds_menu_pager_settings.

This module is set up to handle up to two active site languages, and will
hide next/previous content the user is viewing in a language for which no 
translation exists.

REQUIREMENTS
------------

USWDS theme.
Menu Pager module.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


NOTE ABOUT ACTIVE TRAILS
------------
Menu active trails have been known to cause issues when menu entities are stored 
in multiple menus. See e.g. https://www.drupal.org/project/menu_trail_by_path/issues/2870738. 
This may or may not be fixed on Drupal 9.5x. It may be necessary to patch menu_pager's 
MenuBlock.php to avoid issues with Drupal detecting the incorrect active trail.